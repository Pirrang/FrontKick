# FrontKick Discord
Minimalist reproduction of the dice bot Sidekick killed on duty 
Where most of the syntax will be kept

## Syntax

`/r 1d8 + 4d6` - Roll one octahedron and four hexahedrons.

`/r 1d20+5 # Grog attacks` - Roll dice with a comment.

`/r 2d6>=5` - Roll two hexahedrons and take only the ones that turned greater or equal to five (aka difficulty check). Prints the number of successes.


