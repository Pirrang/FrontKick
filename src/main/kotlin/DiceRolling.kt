import java.security.SecureRandom

fun rollDice(nbRoll : Int,
             limit : Int,
             accumulator : Int = 0,
             generatedValue : MutableList<Int> = mutableListOf()):
        Pair<Int, List<Int>> {

    val random = SecureRandom()
    random.setSeed(random.generateSeed(20))
    var generated = random.nextInt(limit) + 1

    generatedValue.add(generated)

    return if (nbRoll > 1) {
        rollDice(nbRoll-1, limit, accumulator + generated, generatedValue)
    } else {
        Pair(generated + accumulator, generatedValue)
    }
}

fun <T> concatenate(vararg lists: List<T>): List<T> {
    return listOf(*lists).flatten()
}

fun criticalThrow(maxDice : Int, nbOfRoll : Int, dice : Pair<Int, List<Int>>, ceiling : Int): Pair<Int, List<Int>> {
    //TODO change that -_-
    val rethrow = rollDice(nbOfRoll,maxDice)
    val newDiceValue = Pair(rethrow.first + dice.first, concatenate(dice.second, rethrow.second))
    return if (rethrow.first >= ceiling+1) {
        criticalThrow(maxDice, nbOfRoll,newDiceValue,ceiling+1)
    } else {
        newDiceValue
    }
}
