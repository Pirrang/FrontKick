import com.jessecorbett.diskord.dsl.*
import com.jessecorbett.diskord.util.authorId
import com.jessecorbett.diskord.util.words

var currentUser : String = ""
suspend fun setupBotInteractionHandler(token : String) {
    bot (token) {
        val currentBot = this

        commands("/") {
            command("initDice") {
                initInitInit(words.drop(1))
            }
            command("initRoll") {
                if (isStoryteller(currentBot, this)) {
                    reply(initRoll(words.drop(1)))
                } else {
                    reply("You're not storyteller !")
                }
            }
            command("initAdd") {
                if (isStoryteller(currentBot,this)) {
                    initInitInit(words.drop(1),false)
                }else {
                    reply("You're not storyteller !")
                }
            }

            command("r") {
                currentUser = this.author.username
                this.reply("<@${this.authorId}> " + rollDice(words.drop(1).joinToString(" ")))
            }
        }
    }
}