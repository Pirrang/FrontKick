import com.jessecorbett.diskord.api.model.Guild
import com.jessecorbett.diskord.api.model.Message
import com.jessecorbett.diskord.api.model.User
import com.jessecorbett.diskord.dsl.Bot
import com.jessecorbett.diskord.util.authorId

const val COMMAND_PREFIX = "/r "
val initiativeDico : MutableMap<String, Int> = mutableMapOf()


fun isBotCommand(message : String) = message.indexOf(COMMAND_PREFIX) == 0

//first version brute force the parsing brainlessly
//TODO Draw scalable design because been told someone had too much ideas ...
//TODO refactor this shit later
//TODO change the parse design lul

fun formatBotAnswer(com : String, diceValue : Int, op : List<Int>, additionalOps : List<Pair<String, Int>>) : String {
    var finalValue = diceValue
    additionalOps.forEach {
        when(it.first) {
            "+" -> finalValue += it.second
            "-" -> finalValue -= it.second
        }
    }

    var answer = "result ${if (com != "?") "for comment `${com}`:" else ":"} $finalValue ("
    for (i in op) {
        answer += "$i,"
    }
    answer = answer.dropLast(1)
    answer += ")"

    additionalOps.forEach {
        answer += " ${it.first} ${it.second}"
    }

    return answer
}

fun rollDice(message : String): String {
    var command = message
    val comment = if (command.indexOf("#") != -1) command.substring(command.indexOf("#")+1,command.length) else "?"
    if (comment != "?") {
        command = command.substring(0, command.indexOf("#"))
    }
    val splitOperators = command.split("\\s".toRegex())

    //TODO refactor to support a bigger number of dice throws and a more generics ceiling values
    val diceThrowSimulation = splitOperators[0] // we assume that the first operations will always be the dice
    val regex = Regex("[1-9]d(1000|100|10|8|6)") //TODO generalize for more complex dice
    val op = regex.find(diceThrowSimulation)?.value
    if (op != null) {

        val nbOfRoll = op.split("d")[0].toInt()
        val limit = op.split("d")[1].toInt()

        //TODO refactor to support better parsing methods than brute-force
        var diceResult = rollDice(nbOfRoll, limit)
        val rethrow = splitOperators[0]
        //Critical streak
        if (rethrow.contains("!>")) {
            val temp = rethrow.split("!>")[1].toInt()
            val ceilingValue = if (temp < 0) temp * (-1) else temp
            if (diceResult.first >= ceilingValue) {
                //TODO add also critical fail (only critical success are currently supported)
                diceResult = criticalThrow(limit, nbOfRoll, diceResult, ceilingValue)
            }
        }

        //TODO refactor that to support more operations
        val additionalOps = mutableListOf<Pair<String, Int>>()
        for (i in 1 until splitOperators.size) {
            if (splitOperators[i] == "+" || splitOperators[i] == "-") {
                val value = splitOperators[i + 1]
                additionalOps.add(Pair(splitOperators[i], value.toInt()))
            }
        }
        return formatBotAnswer(comment, diceResult.first, diceResult.second.toList(), additionalOps)
    }

    return "invalid operation"
}

fun initInitInit(message : List<String>, overwrite : Boolean = true) {
    assert(message.size % 2 == 0)
    if (overwrite) { initiativeDico.clear() }
    for (i in message.indices step 2) {
        val name = message[i]
        val initiativeValue = when(message[i+1][0]) {
            '+' -> message[i+1].toInt()
            '-' -> message[i+1].drop(1).toInt()*(-1)
            else -> message[i+1].toInt()
        }
        println(initiativeValue)
        initiativeDico[name] = initiativeValue
    }
}


fun initRoll(message : List<String>) : String {

    val ceiling = if (message.isNotEmpty()) message[0].toInt() else 95
    var reply = "Initiative order : "
    val initiativeOrderList : MutableList<Pair<String, Int>> = mutableListOf()
    val resultDetails : MutableMap<String, List<Int>>  = mutableMapOf()
    for (i in initiativeDico) {
        var diceResult = rollDice(1, 100)
        if (diceResult.first >= ceiling) {
            diceResult = criticalThrow(100,1,diceResult,ceiling+1)
        }
        initiativeOrderList.add(Pair(i.key, diceResult.first + i.value))
        resultDetails[i.key] = diceResult.second
    }

    initiativeOrderList.sortByDescending {it.second}
    println(initiativeOrderList)

    var order = 1
    initiativeOrderList.forEach {
        reply += "\n"
        reply += "$order. ${it.first} : ${it.second} -> ${resultDetails[it.first]} + (${initiativeDico[it.first]})"
        order++
    }
    return reply
}

suspend fun isStoryteller(bot : Bot, message : Message) : Boolean {
    val serverId : String = message.guildId!! // cast to string for non-null assertion
    val serverRoles = bot.clientStore.guilds[serverId].get().roles
    val authorRoles = bot.clientStore.guilds[serverId].getMember(message.authorId).roleIds

    serverRoles.forEach { role ->
        if (role.name == "Storyteller") {
            val storytellerId = role.id
            return authorRoles.contains(storytellerId)
        }
    }

    return false
}